const mongoose = require('mongoose');
const User = require('../Models/userSchema.js');

const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Controllers

//User Registration
module.exports.userRegistration = (req, res) => {

		const input = req.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return res.send('The email is already taken!')
			}else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					mobileNo: input.mobileNo
				})

				newUser.save()
				.then(save => {
					return res.send('You are now registered to our website!')
							})
				.catch(err => {
					return res.send(err)
				})
			}
		})

		.catch(err => {
			return res.send(err)
		})
}

//User Authentication 	
module.exports.userAuthentication = (req, res) => {
		let input = req.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return res.send('Email is not yet registered. Register first before logging in!')
			}else{
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return res.send({auth: auth.createAccessToken(result)});
				}else{
					return res.send('Password is incorrect!')
				}
			}
		})

		.catch(err => {
			return res.send(err)
		})
}

//Retrieve User Details
module.exports.getProfile = (req, res) => {
		const userId = req.params.userId;

		User.findById(userId)
			.then(result => {
			result.password = "";

			return res.send(result);
		})
			.catch(error => res.send(error));
}


//Set user as admin (Admin only)
module.exports.updateUser = (req, res) => {
		const userData = auth.decode(req.headers.authorization);
		const userId = req.params.userId;
		const input = req.body

		if(!userData.isAdmin){
			return res.send('You don\'t have access in this page!');
		}else{
			User.findById(userId)
			.then(result =>{
				if (result === null){
					return res.send('User ID is invalid, please try again!')
			}else{
			let updatedUser = {
				isAdmin: true
			}

			User.findByIdAndUpdate(userId, updatedUser, {new: true})
			.then(result => {
				
				return res.send('User has been set as an admin!')})
			.catch(error => res.send(error));
			}
		})
		}
	}	

